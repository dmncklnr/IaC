# Handlungskompetenzen

## Modulspezifische Handlungskompetenzen

* B4 Entwicklungsmethoden zur Lösung von ICT-Problemen und Entwicklung von ICT-Innovationen zielführend einsetzen
  * B4.6 Aktuelle technologiebasierte Entwicklungswerkzeuge einsetzen (Niveau: 3)
  * B4.7 Informationsquellen und Wissensnetzwerke kritisch reflexiv nutzen (Niveau: 3)

* B12 System- und Netzwerkarchitektur bestimmen
  * B12.4 Die Anforderungen an ein Konfigurationsmanagementsystem einer ICT-Organisation erheben und mögliche Lösungsvarianten vorschlagen (Niveau: 3)

* B14 Konzepte und Services umsetzen
  * B14.1 Technische und organisatorische Massnahmen planen und für die Einführung von Software bzw. Releases ausarbeiten (Niveau: 3)
   
## Allgemeine Handlungskompetenzen

* A1 Unternehmens- und Führungsprozesse gestalten und verantworten
  * A1.11 Die Motivation im Team fördern und dieses zu Höchstleistungen befähigen (Niveau: 3)

* A2 Kommunikation situationsangepasst und wirkungsvoll gestalten (Niveau: 3)
  * A2.6 Die branchenspezifischen Fachtermini des Engineerings verwenden und diese adressatengerecht kommunizieren (Niveau: 3)

* A3 Die persönliche Entwicklung reflektieren und aktiv gestalten (Niveau: 3)
  * A3.1 Die eigenen Kompetenzen bezüglich der beruflichen Anforderungen regelmässig reflektieren, bewerten und daraus den Lernbedarf ermitteln (Niveau: 3)
  * A3.2 Neues Wissen mit geeigneten Methoden erschliessen und arbeitsplatznahe Weiterbildung realisieren (Niveau: 3)
  * A3.3 Neue Technologien kritisch reflexiv beurteilen, adaptieren und integrieren (Niveau: 3)
  * A3.4 Die eigenen digitalen Kompetenzen kontinuierlich weiterentwickeln (Niveau: 3)

 

## Anforderungsniveau

Das Anforderungsniveau einer Kompetenz ist durch die Komplexität der zu lösenden Problemstellung, die Veränderlichkeit und Unvorhersehbarkeit des Arbeitskontextes und die Verantwortlichkeit im Bereich der Zusammenarbeit und Führung definiert. HF Absolvierende sind generell in der Lage Problemstellungen und Herausforderungen zu analysieren, diese adäquat zu bewerten und mit innovativen Problemlösestrategien zu lösen. Die Handlungskompetenzen werden in vier Anforderungsniveaus eingestuft.

### Kompetenzniveau 1: Novizenkompetenz

Erfüllen selbständig fachliche Anforderungen; mehrheitlich wiederkehrende Aufgaben in einem überschaubaren und stabil strukturierten Tätigkeitsgebiet; Arbeit im Team und unter Anleitung.

### Kompetenzniveau 2: fortgeschrittene Kompetenz

Erkennen und analysieren umfassende fachliche Aufgabenstellungen in einem komplexen Arbeitskontext und sich veränderndem Arbeitsbereich; führen teils kleinere Teams; erledigen die Arbeiten selbständig unter Verantwortung einer Drittperson.

### Kompetenzniveau 3: Kompetenz professionellen Handelns

Bearbeiten neue komplexe Aufgaben und Problemstellungen in einem nicht vorhersehbaren Arbeitskontext; übernehmen die operative Verantwortung und planen, handeln und evaluieren autonom.

