# Übersicht über Infrastructure as Code (IaC)

[[_TOC_]]

In diesem Teil des Modules IaC werden wir zuerst viele mögliche Facetten von IaC anschauen. Wir wollen uns so einen ersten Überblick verschaffen, was verschiedene Tools wie tun können. Dazu möchten wir  Buzz-Words genauer einordnen, bevor wir dann einzelne Tools genauer anschauen.
In 3 Gruppen werden wir einzelne Aspekte genauer unter die Lupe nehmen, und diese dann den anderen Teilnehmern präsentieren.

In jeder Gruppe sollen 2 Subgruppen gebildet werden. Die "klassische" und die "ChatGPT" Subgruppe. Eine soll klassisch mit Google-Suche und einigen vorgegebenen Links das Thema ausleuchten und dann zusammenfassen. Die andere soll mit geschickten Fragen an "ChatGPT" gute Zusammenfassungen kriegen. Dann soll noch eine Kondensat der beiden Varianten erstellt werden.

Hier der Link zu [ChatGPT](https://openai.com/blog/chatgpt/).

## Gruppe 1 
### Geschichte von Infrastruktur as Code (IaC)

Auftrag: Macht eine Kurze Präsentation zur Geschichtlichen Entwicklung von IaC

Lest als erstes die [Einstiegsgedanken von Kief Morris](https://www.thoughtworks.com/insights/blog/infrastructure-code-iron-age-cloud-age)

Folgenden Themen und Fragen sind als Guideline gedacht und nicht als absolute Wahrheit, d.h. falls ihr noch ein zusätzliches Thema findet (es gibt bestimmt noch Themen die **nicht** hier aufgeführt wurden) nehmt es auch dazu:

- Am Anfang war die  manuelle Installation von Hardware und OS normal. Auch war fast jede Installation ein Unikat, d.h. sie wurde genau einmal so gemacht, vielleicht zwei oder 3mal aber nicht mehr. **Wie schafte man es dass alle Deployments gleich waren?**
- Bald kamen die ersten Automatisierungs-Tools wie [Windows AIK](https://de.wikipedia.org/wiki/Windows_Automated_Installation_Kit), [Solaris Jumpstart](https://en.wikipedia.org/wiki/JumpStart_(software)), [Redhat Kickstart](https://en.wikipedia.org/wiki/Kickstart_(Linux)),.... Diese erlaubten unüberwachte Installation. **Was konnten diese zusätzlich? Waren diese jeweils für mehrere OS verfügbar?**
- Dann kamen ersten Virutalisierungsplatformen ([Xen](https://de.wikipedia.org/wiki/Xen),[KVM](https://de.wikipedia.org/wiki/Kernel-based_Virtual_Machine),[QEMU](https://de.wikipedia.org/wiki/QEMU),[VmWare Server](https://de.wikipedia.org/wiki/VMware#VMware_Server)) mit ihren "Images". [siehe Open Virtualization Format](https://en.wikipedia.org/wiki/Open_Virtualization_Format),[VDMK](https://en.wikipedia.org/wiki/VMDK),[VHD](https://de.wikipedia.org/wiki/Virtual-Hard-Disk-Format),...). **Das ist und war eine anderer Ansatz, welcher?**

- Etwa gleichzeitig kamen die ersten Konfigurationsmanagement-Tools.(z.B. [CFEngine](https://de.wikipedia.org/wiki/CFEngine)), [puppet - Community Edition](https://www.puppet.com/docs/puppet/7/),... ) **Was konnten diese? Waren diese nur für ein oder mehrere OS verfügbar?**

- Darauf folgten dann die ersten Container (z.B. [LXC](https://de.wikipedia.org/wiki/LXC), [Docker](https://de.wikipedia.org/wiki/Docker_(Software)),...) **Was wurde da virtualisiert?**

- Bald darauf wurden die ersten Orchestrierungslösungen erfunden ([Docker-Compose, Docker-Swarm](https://en.wikipedia.org/wiki/Docker_(software)), [Kubernetes](https://de.wikipedia.org/wiki/Kubernetes)) **Welches Problem haben diese Tools gelöst?** 

**Ab welcher Entwicklung konnte man schon von Infrastructure as Code sprechen?**

**Wie alt ist das Thema IaC schon?**


## Gruppe 2:

Auftrag: Stellt eine Präsentation zusammen welche die folgenden Themen gut zusammenfassen soll.

### kontiniuierliches vs. onetime Konfigurationsmangagment

Beim Konfigurieren von VMs gibt es verschiedene Tools. Die einen können während der Installation einen gewisse konfiguration erstellen. Andere können diese auch noch nach der Installation ändern.

Anhand von folgenden Tools:
- [Ansible - Communtity Edition](https://docs.ansible.com/ansible/latest/index.html)
- [puppet - Community Edition](https://www.puppet.com/docs/puppet/7/)
- [Cloud-Init](https://cloud-init.io/)
- [Kickstart by Redhat](https://access.redhat.com/documentation/de-de/red_hat_enterprise_linux/7/html/)

Versucht euch folgende Fragen zu beantworten:

**Können alle IaC-Tools Konfigurationen nach der Installationen anpassen?**

**Gibt es Tools die nur für das einmalige ausführen gedacht sind?**

**Was könnte das mit [Idempotenz](https://www.itwissen.info/Idempotent-idempotent.html) zu tun?**

**Es gibt den push und pull Ansatz. Was versteht man unter diesen?**


### Deklarative vs. Imperative Sprachen?

Als erstes möchten wir das Buzz-Word "deklarative Sprache" ein bisschen genauer anschauen
Als Einstieg [ein Video](https://www.youtube.com/watch?v=E7Fbf7R3x6I) zum Thema. Vielleicht fragt ihr euch, ob es wirklich die Sprache oder mehr die Art wie wir die Sprache benutzen ausmacht, ob wir deklaritiv "programmieren","sprechen" oder "beschreiben? 

#### Links zum Thema
Es ist nicht ganz einfach gute Links zum Thema zu finden. Darum hier ein paar Links zum Thema, die einige Aspekte gut erklären, aber nicht als vollkommen angeschaut werden können:
- [computerweekly.com](https://www.computerweekly.com/de/definition/Deklarative-Programmierung)
- [Educative.io](https://www.educative.io/blog/declarative-vs-imperative-programming)
- [Nordhero.com](https://www.nordhero.com/posts/10-years-iac/)
- [geeksforgeeks.org](https://www.geeksforgeeks.org/difference-between-imperative-and-declarative-programming/)

#### Fragen die zu beantworten sind
**Wie kommunizieren Menschen im Alltag?**

**Gibt es Programmiersprachen welche sowohl deklarativ wie auch imperativ gebraucht werden können?**

**Kann in BASH deklarativ programmiert werden?**

**Welcher Teil von [SQL](https://de.wikipedia.org/wiki/SQL) ist eine imperative Sprache?**

**Warum werden einige Sprachen als imperativ und andere als deklarativ bezeichnet?**

**Was sind die sind die Hauptunterschiede zwischen deklartivem und imperativen programmieren?**

## Gruppe 3:

### Multi-Cloud-Provisioning vs. Single-Cloud-Provisioning

Bei vielen Cloud-Providern gibt es eine möglichkeit deklarativ eine ganze Lansdschaft von VMs, Services, zu deployen wie zum Beispiel:

- [AWS Cloud Formation](https://docs.aws.amazon.com/whitepapers/latest/introduction-devops-aws/aws-cloudformation.html)
- [Azure Resource Manager ARM](https://learn.microsoft.com/en-us/azure/azure-resource-manager/management/overview)
- [Google Deployment Manager](https://cloud.google.com/deployment-manager/docs?hl=de)


Daneben gibt es Multi-Ccloud-Prosioning wie z.B.
- [Terraform](https://www.terraform.io/)
- [Ansible](https://www.ansible.com/integrations/cloud)
- [Pulumi](https://spacelift.io/blog/what-is-pulumi)

Diese können Umgebungen auf mehreren Cloud-Umgebungen deployen, sie sind also nicht an eine Cloud-Platform gebunden.

#### Fragen die zu beantworten sind

**Was sind Vor- und Nachteile von Multi-Cloud-Provisioning?**

**Was sind Vor- und Nachteile von Single-Cloud-Provisioning?**

**Was muss man mit dem ganzen Infrastruktur-Code tun, wenn man von einer Cloud in die andere Wechseln will?**

### Multi-OS-Provisioning vs. Single-OS Provisioning

Es gibt Konfigurationstools die jeweils nur mit einem spezifischen OS funktionieren. z.B.

- [Kickstart by Redhat](https://access.redhat.com/documentation/de-de/red_hat_enterprise_linux/7/html/installation_guide/sect-kickstart-howto)
- [Windows ADK](https://learn.microsoft.com/de-de/windows-hardware/get-started/adk-instal[l)
- [Redhat Satellite](https://www.redhat.com/de/technologies/management/satellite)
- [Windows SCSM](https://learn.microsoft.com/de-de/system-center/scsm/service-manager?view=sc-sm-2022)


Daneben gibt es Multi-OS-Provisioning tools wie
- [Vagrant](https://www.vagrantup.com/)
- [puppet - Community Edition](https://www.puppet.com/docs/puppet/7/puppet_index.html)
- [Ansible - Communtity Edition](https://docs.ansible.com/ansible/latest/index.html)
- [Chef](https://www.chef.io/)

#### Fragen die zu beantworten sind

**Was sind Vor- und Nachteile von Multi-OS-Provisioning?**

**Was sind Vor- und Nachteile von Single-OS-Provisioning?**

**Was muss man mit dem ganzen Infrastruktur-Code tun, wenn man von einem OS in die andere Wechseln will?**


### Proprietäre vs. community-driven Open-Source-Tools

Man findet viel geschriebenes zu *proprietären vs. Open Source Tools*. Aber das ist eigentlich nicht ganz korrekt so. Es gibt *Open Source Tools* die *proprietär* sind, wie z.B. [Threema](https://threema.ch/de).
Die richtige Unterscheidung wäre *community-driven Tools* vs *prorpietäre Tools*.

Sucht euch selbst ein bild zu machen. Hier ein paar links zu Tools.

proprietäre Tools

- [Docker-Hub](https://hub.docker.com/)
[Redhat-Openshift](https://www.redhat.com/en/technologies/cloud-computing/openshift)
- [VMware Tanzu Application Platform](https://tanzu.vmware.com/application-platform)
- [puppet-Entrerprise](https://www.puppet.com/products/puppet-enterprise)
- [Red Hat Ansible Automation Platform](https://www.redhat.com/en/technologies/management/ansible)



communtiy-driven Tools
- [Gitlab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/)
- [Cloud-Init](https://cloud-init.io/)
- [podman](https://podman.io/)
- [Kubernetes](https://kubernetes.io/de/)
- [LXC](https://linuxcontainers.org/)
- [puppet - Community Edition](https://www.puppet.com/docs/puppet/7/puppet_index.html)
- [Ansible - Communtity Edition](https://docs.ansible.com/ansible/latest/index.html)

#### Fragen die zu beantworten sind

**Was hiesst proprietär?**

**Ist proprietär wirklich das Gegenteil von Open-Source?**

**Vergleicht community-driven mit der [Allmend](https://de.wikipedia.org/wiki/Allmende). Was fällt euch auf?**

**Ist [Terraform](https://www.terraform.io/) proprietär oder community-driven?**

**Ist Open-Source gleich "free"?**

**Welche Vorteile hat ein community driven Tool?**

**Welche Nachteile hat ein community driven Tool?**

**Welche Beispiele von Open-Source aber nicht "free" Software gibt es noch ausser [Threema](https://threema.ch/de)?**

**Gibt es Produkte die Open-Source und communtiy-driven und trotzdem Proprietär sind?**
