# Toollandschaft IaC

[[_TOC_]]



In diesem Abschnitt möchten wir uns einen groben Überblick über die aktuellen IaC-Tools nachen. Diese werden in zehn Jahren nicht mehr dieselben sein, das ist ziemlich sicher, aber deren Funktionsweise ähnlich oder gleich wie heute. Darum macht es Sinn die heutigen Tools nach ihren Eigenschaften richtig einzuordnen.
Das wollen wir Anhand der Kriterien die wir in der [Übersicht](../A_Uebersicht_IaS/README.md) kennengelernt haben tun. Wir möchten jedes dieser Tools diesen Kriterien zuordnen.
In Zukunft wird es sehr wahrscheinlich noch neue Kriterien geben, die die Tools unterscheiden, denn mit jedem neuen Tool das dazukommt, gibt es potentiell auch eine völlig neue Art etwas zu tun.

# Auftrag

Schaut die Tools in dieser Liste an:

- [Terraform](https://www.terraform.io/)
- [Ansible](https://www.ansible.com/)
- [Chef](https://www.chef.io/)
- [Puppet](https://puppet.com/)
- [Pulumi](https://www.pulumi.com/)
- [SaltStack](https://www.saltstack.com/)
- [Juju](https://juju.is/)
- [OpenStack Heat](https://docs.openstack.org/heat/latest/)
- [Nomad](https://www.nomadproject.io/)
- [AWS CloudFormation](https://aws.amazon.com/cloudformation/)
- [Google Cloud Deployment Manager](https://cloud.google.com/deployment-manager)
- [Azure Resource Manager](https://azure.microsoft.com/en-us/features/resource-manager/)
- [Kubernetes](https://kubernetes.io/)
- [Docker Compose](https://docs.docker.com/compose/)
- [Cloud Init](https://cloud-init.io/)
- [Vagrant](https://www.vagrantup.com/)
- [Multipass](https://multipass.run/)
- [Cloudify](https://cloudify.co/)

Falls ihr noch ein Zusätzliches Tool findet/kennt, dann nehmt dieses auch noch in die Liste auf.

Beurteilt für diese folgende Kriterien, falls möglich:

- Ist es eher eine deklarative oder imperative Sprache?
- Ist es Proprietär?
- Kontinuierliches Konfigurationsmanagement?
- Existiert es mehr als 5 Jahre?
- Gibt es eine "free" Version davon?
- Ist das Tool nur bei einen Cloud-Provider einsetzbar?
- Ist das Tool nur für das Deployment bzw. die Konfiguration von einem bestimmten OS gedacht?
- Unterstützt das Tool Deployment von Containern und/oder VMs?
- Ist das Tool nur auf einem bestimmten Betriebssystem verfügbar?
- Welche anderen IaC tools können integriert werden?

Und fasst dies in einem Excel Sheet zusammen.