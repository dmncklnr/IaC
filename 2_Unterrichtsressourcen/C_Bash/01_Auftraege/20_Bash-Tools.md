Benutze jeweils die erwähnten Tools die Aufgabe zu lösen, auch wenn es anders auch ginge.

- `cat`: Benutze `cat` um die drei feils /etc/hosts, /etc/group, /etc/passwd zu einem File $HOME/meinfile zusammenzuhängen.
- `head`: Gebe die ersten 3 Zeilen von /etc/resolv.conf aus.
- `tail`: Gebe die letzten 3 Zeilen von /etc/resolv.conf aus.
- `tail`: Gebe alle Zeilen ab Zeile 4 von /etc/resolv.conf aus.
- `sort`: Sortiere das passwd file nach username absteigend, d.h. von z zu a
- `sort`: Sortiere das passwd file nach userid (3. Spalte) aufsteigend
- `grep`: Filtere das File passwd nach allen Usernamen(1. Spalte) die mit 't' anfangen.
- `cut`: erzeuge eine Liste die nur die Usernamen in passwd enthält.
- `wc`: Zaehle die Anzahl Zeilen in /etc/group
- `wc`: Zaehle die Anzahl Worte und Anzahl Zeilen in /etc/hosts mit einem Kommando.


**Advanced Aufgaben:**
- `grep`: Filtere das File passwd nach allen Usernamen(1. Spalte) die mit `t` anfangen und `p` enden.
- `sort`,`cut`: erstelle eine Liste der in /etc/passwd benutzten Gruppen-Ids (spalte 4). Jede Gruppen-Id soll nur einmal aufgeführt sein.
- `sort`,`head`,`tail`: Nehme die ersten 3 Zeilen von /etc/group und die letzten 3 zeilen von /etc/passwd und sortiere diese
- `grep`,`cut`: Gebe die Userid(Spalte 3. in /etc/passwd) vom User mit Usernamen `tcpdump` aus.

[Lösungen](../03_Loesungen/handson_basic_tools.md)
