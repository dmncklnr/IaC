In diesem Auftrag sollt ihr `sed` verstehen lernen, indem ihr 4 Kommandos ausführt und versucht herauszufinden was dann geschieht.
Was tun die folgenden Kommandos? 
```
sed '/Redistribution and use/,/are met:/ s/conditions/CONDITIONS/g' /usr/share/common-licenses/BSD >BSD
sed -i.backupcopy '/Redistribution and use/,/are met:/ s/CONDITIONS/Conditions/g' BSD
```
Führt diese beiden Kommandos in eurer VM aus.
Versucht die Unterschiede des Files `BSD` und `/usr/share/common-licenses/BSD` zu verstehen nachdem ihr das erste Kommande ausgefuehrt habt.
Dann nach dem 2. Kommando koennt ihr noch die unterschiede in `BSD.backupcopy` und `BSD` vergleichen.

Wie vergleicht ihr diese Files?

Was passiert wenn ihr `/Redistribution and use/,/are met:/` weglässt, d.h. einfach

```commandline
sed 's/conditions/CONDITIONS/g' /usr/share/common-licenses/BSD >BSD
sed -i.backupcopy 's/CONDITIONS/Conditions/g' BSD

```
ausführt?
