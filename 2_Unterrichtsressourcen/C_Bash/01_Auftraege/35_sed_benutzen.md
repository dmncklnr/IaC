In diesem Auftrag sollt ihr selst ein `sed`-Kommando benutzen um ein File zu modifizieren.

Modifiziert mit einem `sed`-Kommando das File `/etc/ssh/sshd_config` so, 
dass nur noch `.ssh/authorized_keys` benutzt wird und nicht mehr die beiden Files `.ssh/authorized_keys` und `.ssh/authorized_keys2`

Was wäre die bessere Lösung als das Konfig-File direkt zu editieren?
