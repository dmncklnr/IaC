In diesem Auftrag sollst du mit `awk` typische probleme in der Bash-Programmierung lösen:

- Gebe alle Usernamen in /etc/passwd aus die eine User-ID >= 1000 haben
- Gebe die Anzahl Einträge in /etc/passwd aus, welche die Login-Shell(letzte Spalte) `/usr/sbin/nologin` haben und User-ID <=900. (Anmerkung: das könnte man auch mit `grep` und `wc` machen, es braucht aber eine ziemlich komplizierte Regular-Expression)

Hast du den einzeler mit `awk` gemacht?

Kannst du das auch mit einem Pure-Shellskript ohne Kommandos (Tip: while read ...)?

