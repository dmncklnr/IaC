# Bourne Again-Shell

[[_TOC_]]

Bash steht für "Bourne-Again Shell" und ist eine Kommandozeileninterpreter-Shell für Unix- und Unix-ähnliche Betriebssysteme wie Linux oder macOS, und heute sogar für Windows. 

Bash ist ein unverzichtbares Werkzeug für die Automatisierung. In der IaC (Infrastructure-as-Code) ist es von entscheidender Bedeutung, kurze Prozesse zu automatisieren, wofür oft die Bash-Programmierung eingesetzt wird. Manchmal müssen jedoch auch komplexe Abläufe in Bash programmiert werden, was uns dazu zwingen kann, unsere Fähigkeiten in der Bash-Programmierung zu vertiefen.

Bash bietet eine Vielzahl von Sprachelementen, wie z.B. Variablenzuweisungen, Bedingungen, Schleifen, Funktionen und mehr, die zur Erstellung von Shell-Skripten verwendet werden können.

Diese wollen wir hier kennenlernen. 

Zusätzlich wollen wir noch die wichtigsten Unix Tools kennenlernen, welche beim BASH-Skripting benutzt werden.

Ich habe euch auch noch folgendes [Cheatsheet](../Z_Cheatsheets/bash) zusammengestellt und werde dieses laufend ergänzen.

## Kompetenzen

- B4.6  	Aktuelle technologiebasierte Entwicklungswerkzeuge einsetzen (KN:3)  

## Lernziele / Taxonomie 

- Ich kenne die wichtigsten Sprachelemente von BASH und kann diese anwenden.
- Ich kann Abläufe mit BASH-Skripten automatisieren.
- Ich beherrsche die Fähigkeit, Skripte für Input-Output-Prozesse zu erstellen.
- Ich kenne die wichtigsten Unix-Tools die beim erstellen von BASH-Skripten benutzt werden.


## Einführungstutorial

In diesem 
[Tutorial](https://linuxconfig.org/bash-scripting-tutorial-for-beginners) lernst du die wichtigsten Sprachelemente von BASH kennen.

### Kompetenzen

B4.6  	Aktuelle technologiebasierte Entwicklungswerkzeuge einsetzen (KN:3)  

### Lernziele / Taxonomie 

- Ich kenne die wichtigsten Sprachelemente von BASH und kann diese anwenden.
- Ich kann einfache Abläufe mit BASH-Skripten automatisieren.



### Hands-on

1. Erstelle eine VM mit multipass und mache die Übungen im [Tutorial](https://linuxconfig.org/bash-scripting-tutorial-for-beginners) mit.

2. Erstelle ein Restore-Skript welches die Backups, die mit dem *Backup Skript vom Tutorial* erstellt wurden, wiederherstellt. Du sollst dabei einen oder mehrere User angeben können für welche das Backup wiederhergestellt werden sollen. <br><br>[Einfache Lösung](03_Loesungen/restore.sh)

3. [Zusatzübungen für Bash-Beginners](01_Auftraege/10_Bash-Beginners-Uebungen.md)



## Tools

### Lernziele / Taxonomie

- Ich beherrsche die Fähigkeit, Skripte für Input-Output-Prozesse zu erstellen.
- Ich kenne die wichtigsten Unix-Tools die beim erstellen von BASH-Skripten benutzt werden.

### Basic Tools
Zu jeden dieser Basic Tools die du beim Skripten brauchen wirst habe ich dir eine Tutorial verlinkt.

- [cat](https://www.unixtutorial.org/commands/cat): File lesen und Ausgeben
- [head/tail](https://www.baeldung.com/linux/head-tail-commands): nur die ersten oder letzten x Zeilen von File ausgeben
- [sort](https://shapeshed.com/unix-sort/): File sortieren nach verschiedenen Kriterien.
- [grep](https://ryanstutorials.net/linuxtutorial/grep.php): File filtern nach pattern.
- [cut](https://www.tutorialspoint.com/unix_commands/cut.htm): Einzelne “Spalten” aus File rauslesen
- [wc](https://www.tutorialspoint.com/unix_commands/wc.htm): Zeilen, Worte oder Buchstaben in File zählen


#### Hands-on

[Auftrag Tools](01_Auftraege/20_Bash-Tools.md)

### Stream-Line-Editor `sed`: Einführung

`sed` ist eines der wichtigsten Unix Tools wenn es im Filemanipulationen geht. Darum sollte wir lernen damit umzugehen. Das erleichtert uns z.B. Konfigfiles automatisiert auszufüllen.

Eine Tutorial findet ihr unter [Einführung by Digital Ocean](https://www.digitalocean.com/community/tutorials/the-basics-of-using-the-sed-stream-editor-to-manipulate-text-in-linux)

Und hier findet ihr noch ein kurzes [Einführungs-Video](https://www.youtube.com/watch?v=Sz0xvKUmzpk)

#### Hands-on

[Auftrag: sed Verstehen](01_Auftraege/30_sed_verstehen.md)  
[Auftrag: sed Benutzen](01_Auftraege/35_sed_benutzen.md)

### Links

[Kurz-Refernenz SED](https://quickref.me/sed)

[Advanced Tutorial](https://www.grymoire.com/Unix/Sed.html)

### Das Tool `awk`: Einführung

Chat GPT sagt: *Awk ist eine Programmiersprache für die Verarbeitung und Analyse von Textdateien. Es verwendet reguläre Ausdrücke und bietet Kontrollstrukturen wie Schleifen und Bedingungen. Awk ist auf den meisten Unix-basierten Betriebssystemen verfügbar und wird häufig für Systemadministration, Datenaufbereitung und Textverarbeitung eingesetzt.*

Wenn du mit den oben genannten Tools an Grenzen stösst, dann ist manchmal `awk` das richtig Tool um das Problem zu lösen. Es hat eine eigene Programmiersprache, die ziemlich mächtig ist. Es ist auf allen Unix-Derivaten standardmässig installiert.

Wichtige Anwendungen sind zum Beispiel: 
- Den Durchschnitt, Maximum, Minimum oder Summe einer Spalte in einem File berechnen. 
- Gewisse Spalten von einem File ausgeben, aber nur wenn in anderen Spalten des Files gewisse Kriterien erfüllt sind.

Aber das Tool ist viel mächtiger. Trotzdem warne ich vor allzugrossen Anwendungen davon, da es dann meistens sinnvoller, ist eine andere Programmiersprache, wie z.B. Python zu benutzen.

In den nachfolgenden Tutorials werden ziemlich viele Möglichkeiten von `awk` behandelt. Ich schlage dir vor nur einen Teil der Tutorials anzuschauen.

[Tutorial, aber nur bis und mit *Kapitel 5: Search Patterns*](https://www.baeldung.com/linux/awk-guide)

[Tutorial, aber nur bis und mit *Kapitel: AWK Fields*](https://zetcode.com/lang/awk/)

#### Hands-on
[Auftrag awk](01_Auftraege/40_awk_benutzen.md)
## Relozierbare Skripts

Warum soll man ein Skript so schreiben das es relozierbar ist? 
Der wichtigste Grund ist, dass ein Skript vielleicht einmal in einem anderen Verzeichnis installiert werden soll. 
Dann ist es ziemlich aufwändig, wenn man das Skript anpassen muss.

Mit ein paar kleinen Tricks kann man das verhindern.

Um das geht es hier. Der folgende Code setzt die Variabeln für wichtige Directories
unabhaengig vom Installationsort richtig:

```bash
cwd=`pwd`        # current working directory  
BINDIR=$(cd `dirname $0`;`pwd`)   # BINDIR: the directory where the script is located
BASENAME=`basename $0`    # Set the script name (without path to it)
TMPDIR=/tmp/$BASENAME.$$    # Set a temporary directory if needed with a ending $$ which is the process-ID of the Skript
ETCDIR=$BINDIR/../etc        # ETCDIR is the config directory of the script, which is normally located one directory up from the script location and then into etc
```

Diesen Code kann man in ein File `common_variables.bash` im Skript-Directory versorgen und dann dieses am Anfang vom jedem skript *sourcen*
```bash
#!/bin/bash

source $(dirname $0)/common_variables.bash
```
und schon hat man immer diese Variablen gesetzt und der Code ist immer derselbe, egal wo das Skript und das `common_variables.bash` abgelegt wird.
Wenn man jetzt im Skript ein zweites Skript im Skript-Verzeichnis aufrufen will kann man das einfach tun indem man:

```bash
$BINDIR/skript2.bash
```
braucht. Auch das `skript2.bash` wird am Anfang die Zeile `source $(dirname $0)/common_variables.bash` haben.

#### Hands-on

[Erzeugen von Relozierbaren Skripten](01_Auftraege/50_relozierbare_skripts.md)  
[Auftrag Logfunktion](01_Auftraege/55_logfunction.md)

## Transfer

[Aufgabe um den Transfer zu IaC zu kriegen](../D_Cloud-Init/01_Auftraege/40_apache_php_wordpress_cloudinit.md)



## Links

[Bash Cheatsheet by Armin Doerzbach](../Z_Cheatsheets/bash)

[Übersicht über die gängigsten Unix-Commands](https://www.unixtutorial.org/basic-unix-commands)

[Buch "Shell Programmierung" vom Rheinwerk-Verlag](https://openbook.rheinwerk-verlag.de/shell_programmierung/)

[Code Snippets in Pure-Bash für gängige Probleme](https://github.com/dylanaraps/pure-sh-bible)

