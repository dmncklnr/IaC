# Cloud-Init Internals

## Ubuntu- vs. multipass cloud-init config vs. AWS-cloud-init config

Im Video wurde euch gezeigt wie man eine *Standard-Ubuntu-Cloud-Init-Konfiguration* herstellen kann, wenn *cloud-init* auch schon gelaufen ist. Euer Auftrag ist nun die *Cloud-Init-Konfiguration* die mit *multipass* angepasst wurde, mit der *Standard-Ubuntu-Cloud-Init-Konfiguration* zu vergleichen.

- Dazu setzt ihr am besten nochmals eine neue VM auf, wie ihr es im Auftrag [VM mit einfacher Cloud-Init-Konfiguration erstellen](./10_VM_mit_einfacher_cloudinit_config.md) gemacht habt.
- Dann benennt (Unix-Kommand: mv) ihr das Konfiguration-Verzeichnis /etc/cloud auf /etc/cloud.multipass um.
- Dann macht ihr die ersten Schritte vom Video um *cloud-init* zu deinstallieren und wieder neu zu installieren.
- Jetzt könnt ihr die Inhalte der beiden Verzeichnisse /etc/cloud und /etc/cloud.multipass vergleichen. Was fällt euch auf?
- Macht dasselbe auf der AWS-Cloud. Was fällt euch auf?

## Startup Prozess

Wie wird eigentlich cloud-init beim Booten gestartet?

In [https://cloudinit.readthedocs.io/en/latest/explanation/boot.html](https://cloudinit.readthedocs.io/en/latest/explanation/boot.html) wurde von einem Generator gesprochen. Aber wie finde ich was dieser macht?

Unter `/usr/lib/systemd/system-generators/` findet man alle möglichen Generator und auch unseren 'cloud-init-generator'. Das ist ein BASH-Skript. Welches genau das macht was in der Doku beschrieben ist, und noch ein bisschen mehr. Es führt nämlich das Kommando `/usr/lib/cloud-init/ds-identify` aus. Dieses hat die Aufgabe, die richtige *Datasource* für die Platform (z.B Azure, Ec2, GCP, OVM,....) worauf die VM läuft zu identifizieren. Das geschieht mit ganz viel undurchsichtigem *Voodoo*. Hoffen wir dass es immer richtig funktioniert. Das Resultat wird unter `/run/cloud-init/cloud.cfg` abgelegt.

Was geschieht danach?
Findet die entsprechenden Services mit: 
```
systemctl|grep cloud-.*service
```
und ordnet diese den Stages die im obigen Dokument beschrieben sind zu.

Falls ihr genau wissen wollt, in welcher Reihenfolge die Services gestarted werden, könnt ihr in die entsprechenden Files unter `/lib/systemd/system` schauen. Dort werden Abhängigkeiten definiert und es wird bestimmt was wie ausgeführt wird. Mit `systemctl list-dependencies cloud-....service` werden die Abhängigkeiten als *Baum* aufgezeichnet.

## Änderungen in der Cloud-Init-Config

Versucht nun die Configuration so anzupassen, dass die module set-passwords und users-groups bei jedem Reboot ausgeführt werden. Instruktionen findet ihr in [Change how often a module runs](https://cloudinit.readthedocs.io/en/latest/howto/module_run_frequency.html)

Jetzt versucht noch sicherzustellen, dass du module puppet und ansible ganz sicher nicht laufen.

[Lösung für diese Aufgabe](30_internals_part1_loesung.md)


