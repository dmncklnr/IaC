# Lösung: Cloud-Init Internals

## Änderungen in der Cloud-Init-Config

Erzeugt ein File /etc/cloud/cloud.cfg.d/89_meinemodifiktaionen.cfg mit dem Inhalt:
```
cloud_init_modules:
 - migrator
 - seed_random
 - bootcmd
 - write-files
 - growpart
 - resizefs
 - disk_setup
 - mounts
 - set_hostname
 - update_hostname
 - update_etc_hosts
 - ca-certs
 - rsyslog
 - [ users-groups, always ] 
 - ssh

cloud_config_modules:
 - wireguard
 - snap
 - ubuntu_autoinstall
 - ssh-import-id
 - keyboard
 - locale
 - [ set-passwords , always ]
 - grub-dpkg
 - apt-pipelining
 - apt-configure
 - ubuntu-advantage
 - ntp
 - timezone
 - disable-ec2-metadata
 - runcmd
 - byobu

cloud_final_modules:
 - package-update-upgrade-install
 - fan
 - landscape
 - lxd
 - ubuntu-drivers
 - write-files-deferred
# - puppet
 - chef
# - ansible
 - mcollective
 - salt-minion
 - reset_rmc
 - refresh_rmc_and_interface
 - rightscale_userdata
 - scripts-vendor
 - scripts-per-once
 - scripts-per-boot
 - scripts-per-instance
 - scripts-user
 - ssh-authkey-fingerprints
 - keys-to-console
 - install-hotplug
 - phone-home
 - final-message
 - power-state-change
```
**Wichtig**: Kopiert zuerst die ganzen Sektionen `cloud_final_modules`,`cloud_config_modules`,`cloud_init_modules` aus dem File `/etc/cloud/cloud.cfg` und macht dann die Modifikationen.

