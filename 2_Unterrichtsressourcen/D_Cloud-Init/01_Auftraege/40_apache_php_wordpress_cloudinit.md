# Aufsetzen von einem Wordpress-Server

Benutze folgende Module um einen apache-php-wordpress-server zu installieren und starten:

- [package-update-upgrade-install](https://cloudinit.readthedocs.io/en/latest/reference/modules.html#package-update-upgrade-install)
- [write-files](https://cloudinit.readthedocs.io/en/latest/reference/modules.html#resolv-conf)
- [runcmd](https://cloudinit.readthedocs.io/en/latest/reference/modules.html#runcmd)

Benutze dazu Shell-Skripte um die Konfiguration von Wordpress und Mysql vorzunehmen. Dazu solltest du zumindest einen Teil der [BASH-Tutorials gemacht haben](../../C_Bash/README.md).


Du musst damit folgende Komponenten installieren:

- [MariaDB](https://wiki.ubuntuusers.de/MariaDB/)
- [PHP & Apache](https://wiki.ubuntuusers.de/PHP/)
- [Wordpress](https://wordpress.org/documentation/article/how-to-install-wordpress/)

