# Erstellen von Gitlab-Credentials

Credentials die mit der User-Data übergeben werden sollen nur kurzfristig gültige credentials enthalten, d.h. wir schauen, dass kurz nach der Installation die Credentials nicht mehr gültig sind.
Wie erstellen wir solche vollkommen automatisiert?

Dazu gibt es je nach Subscription bei Gitlab verschiedene Optionen.

- temporäre ssh-keys generieren und beim User hinterlegen
- temporäre access-tokens generieren und beim User hinterlegen
- temporäre access-tokens beim Projekt hinterlegen (needs subscription)
- ...

Dazu benutzen wir das [Gitlab-Rest-API](https://docs.gitlab.com/ee/api/rest/).

Hier die Steps die ihr machen müsst:

- [Erzeugen von einem Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) mit Scope `api` welches lange gültig ist.
- Schreibt ein Shell-Skript welches:
  - ein SSH-key-Keypaar generiert (ssh-keygen)
  - den public SSH-key per [Rest-Api](https://docs.gitlab.com/ee/api/users.html#add-ssh-key) dem User hinzufügen.

