# VM erstellen

In diesem Auftrag benutzt ihr die User-Data, die ihr Auftrag 3 erstellt habt, um eine VM zu deployen.

Das könnt ihr mit multipass machen:

```
#!/bin/bash

multipass launch -n securevm --cloud-init /tmp/user-data.yml
``` 

oder mit AWS- bzw. Azure-Cli.

