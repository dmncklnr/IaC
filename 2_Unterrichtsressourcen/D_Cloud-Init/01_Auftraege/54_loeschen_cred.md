# Löschen der credentials

Bevor ihr die SSH-Keys löscht auf gitlab müsst ihr sicherstellen, dass die VM up und running ist und cloud-init beendet ist. Das könnt ihr mit einem loop machen in einem Skript machen.

```
#Warten bis cloud-init finished und disabled wurde
until multipass exec securevm -- test -f /etc/cloud/cloud-init.disabled
do
  sleep 5
done

# Hier kommt der code zum löschen des SSH-Public-Keys auf gitlab
```

Dann müsst ihr den Public SSH-Key den ihr im Auftrag 2 in gitlab installiert habt wieder löschen.
Dazu müsst ihr wieder auf das [Rest-API](https://docs.gitlab.com/ee/api/users.html#delete-ssh-key-for-current-user) von Gitlab zugreifen.
