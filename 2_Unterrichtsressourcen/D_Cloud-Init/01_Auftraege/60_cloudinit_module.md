# Cloud-Init-Module benutzen um VM zu konfigurieren

Ziel dieses Auftrages ist es dass ihr die Standard-Module benutzt um ganz normale VMs zu konfigurieren.

Setzt eine VM mit Multipass auf, die mit einem user-data.yml 
folgende Sachen konfiguriert:

- ssh: Ihr sollt auf der VM euren Public-Key im authorized_keys File hinterlegen, sodass ihr ohne Passwort als ubuntu user einloggen könnt.
- runcmd: 
  - clonen sie das gitlab Repo `https://gitlab.com/armindoerzbachtbz/wordpress.git` ins Verzeichnis /opt/wordpress
  - Disabeln sie cloud-init am Schluss
- locale: Setzen sie die locale de_CH
- ntp: Setzen sie den ntp server `0.ch.pool.ntp.org` als ntp server
- packages:
  - Installieren sie `git`
  - Upgraden sie alle packages auf den letzten Stand
- power_state: 
  - Machen sie einen Reboot 2 Minuten nach erfolgreicher Installation
- resolv_conf:
  - Konfigurieren sie die DNS-Server von google.
- vendor_data:
  - Schalten sie das benutzen der Vendor-Data ab.
- timezone:
  - Setzen sie die Zeitzone auf `US/Pacific`
- users: erstellen sie eine Gruppe `hf_tbz` und die User `ihrvorname` mit der Primary Gruppe `hf_tbz` und der zusätzlichen Gruppe `sudo` und der Berechtigung sudo ohne Passwort auszuführen.
- write_files:
  - Erzeugen sie ein File `/etc/motd` mit dem folgenden Inhalt:
    ```
    It is a lovely day today. Please be sure you want really to work.
    The system is under surveillance. Logout immediately if the sun is shining.
    ```


Eine mögliche Loesung finden sie unter [60_cloudinit_module_loesung.yml](./60_cloudinit_module_loesung.yml).