# Ablauf Cloud-Init Internals

[TOC]

In diesem Teil möchte ich mit euch nochmals den ganzen Startup-Prozess durchgehen wie Cloud-Init Step by Step eine VM konfiguriert.

Wie ihr schon [Cloud-Init Internals (Part1)](#cloud-init-internals-part-1) gesehen habt gibt es 5 Stages.

- Generator (`/usr/lib/systemd/system-generators/cloud-init-generator`)
- Local (`cloud-init-local.service`)
- Network (`cloud-init.service`)
- Config (`cloud-config.service`)
- Final (`cloud-final.service`)

Diese werden wir nun nochmals genauer unter die Lupe nehmen.

## Stage 0: Generator

In diesem Stage wird von systemd das script `/usr/lib/systemd/system-generators/cloud-init-generator` gestartet. 

### Start or not to Start?
Als erstes wird in diesem Script entschieden, ob cloud-init überhaupt gestartet werden soll und zwar so:

- Wenn als Kernel-Boot-Parameter (in grub oder so) `cloud-init=<enabled|disabled>` übergeben wurde, wird anhand von diesem entschieden ob cloud-init services enabled oder disabled werden. (Priorität 1)
- Wenn in `/etc/cloud/cloud-init.disabled` oder `/etc/cloud/cloud-init.enabled` existiert, wird anhand von diesem entschieden ob cloud-init services enabled oder disabled werden. (Priorität 2)
- Sonst werden per default cloud-init services enabled.

D.h. zum Disablen von Cloud-init muss man einfach ein File `/etc/cloud/cloud-init.disabled` erstellen. Dann sollte cloud-init nicht mehr gestartet werden.

### Auf welcher Cloud laufe ich? Welche Datasource soll ich nehmen?


Als nächstes wird mit dem Script `/usr/lib/cloud-init/ds-identify` geschaut auf welcher Cloud die VM läuft und dann die richtigen Datasourcen in `/run/cloud-init/cloud.cfg` geschrieben. Wie genau das geschieht ist ziemlich undurchsichtig (ich würde sagen unschöner Voodoo-Zauber), aber basiert grössten Teils auf Inhalte in Files die unter `/sys/...` gespeichert sind.

### Enabeln von cloud-init services

Als letztes wird ein symbolischer link von
`/run/systemd/generator.early/multi-user.target.wants/cloud-init.target` auf `/lib/systemd/system/cloud-init.target` gemacht, was dann automatische alle cloud-init.services startet.

Die Services die dabei gestartet werden kann man so herausfinden:

```
ubuntu@test:/run/cloud-init$ systemctl list-dependencies cloud-init.target
cloud-init.target
● ├─cloud-config.service
● ├─cloud-final.service
● ├─cloud-init-hotplugd.socket
● ├─cloud-init-local.service
● └─cloud-init.service
```

## Stage 1: Local (cloud-init-local.service)

In diesem Teil wird die Datasource gelesen. Dazu wird bei gewissen Datasourcen eine temporäre Netzwerkkonfiguration erstellt.

Die erste permanente Netzwerkkonfig wird dann aufgrund der Meta-Daten erstellt.

Dazu wird der Service `cloud-init-local.service` gestartet. Der Service führt dann `/usr/bin/cloud-init init --local`, was man auch auf der Kommandozeile tun kann, wenn man etwas testen will.

## Stage 2: Network

In diesem Stage werden alle Module die in der cloud-init config (default in: /etc/cloud/cloud.cfg) in der liste `cloud_init_modules:` stehen abgearbeitet.

Normalerweise werden hier die Files die in cloud-init config mit `write_files:` definiert wurden erstellt. Ssh-keys werden erstellt. Users und Groups werden erstellt. /etc/hosts wird upgadated, ....

Dazu wird der Service `cloud-init.service` gestartet. Der Service führt dann `/usr/bin/cloud-init init`, d.h. das gleiche Skript wie in *Stage 1: Local*, einfach mit anderen Parametern.


## Stage 3: Config

Hier geschieht das was schon eine funktionierende Netzwerkverbindung braucht.

In diesem Stage werden alle Module die in der cloud-init config (default in: /etc/cloud/cloud.cfg) in der liste `cloud_config_modules:` stehen abgearbeitet. Die Module runcmd, wireguard, ntp, ... werden hier ausgeführt.

Dazu wird der Service `cloud-config.service` gestartet. Der Service führt dann `/usr/bin/cloud-init modules --mode=config`, d.h. das gleiche Skript wie in *Stage 1: Local*, einfach mit anderen Parametern.

## Stage 4: Final

In diesem Stage werden alle Module die in der cloud-init config (default in: /etc/cloud/cloud.cfg) in der liste `cloud_final_modules:` stehen abgearbeitet. Die Module package-update-upgrade-install, puppet, ansible, ... werden hier ausgeführt.



Dazu wird der Service `cloud-final.service` gestartet. Der Service führt dann `/usr/bin/cloud-init modules --mode=final`, d.h. das gleiche Skript wie in *Stage 1: Local*, einfach mit anderen Parametern.


