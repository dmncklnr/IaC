# Auftrag 1: Ein einfaches Inventory und Playbook erstellen

## Inventory
Erstellen sie ein Inventory für 3 Server, für ich ihnen per Teams die Zugansdaten zur Verfügung stelle. Erstellen sie eine Gruppe `mymanagednodes`. Setzen sie die Variable `ansible_user`, damit ansible automatische mit dem richtigen User einloggt. Erstellen sie je ein Inventory-File im ini-Format und eines im yaml-Format.

[Lösung Inventory](Auftrag_1_Loesung_Inventor.md)

Testen sie ihre Inventories mit:
```
ansible --list-hosts -i <Inventoryfile> all 
ansible --list-hosts -i <Inventoryfile> mymanagednodes
```
Es sollte zweimal denselben Output liefern.

## Connection-Check

Versuchen sie ob sie die Hosts im Inventory erfolgreich per Ansible ansprechen können. Benutzen sie das Ansible Module `ping` dazu
```
ansible -i <Inventoryfile> -m ping <Gruppenname>
```
Was passiert? Warum?

Haben sie das erste Problem gelöst?

Dann versuchen sie es noch einmal.

Was passiert? Warum?

Benutzen sie den SSH-Private-Key `iackey` den ich ihnem per Teams zur verfügung stelle um sich zu einzuloggen. 
```
ansible --private-key iackey -i <Inventoryfile> -m ping <Gruppenname> 
```

# Playbook erstellen

Playbook erstellen um genau ein File mit deinem Namen in `/` zu erstellen.

Benutzen sie ganz einfach das Ansible Module [`file`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html#examples)

Führen sie das Playbook aus:
```
ansible-playbook  --private-key iackey -i <Inventoryfile> <playbookfile>
```

warum funktioniert es nicht?

Füge die richte Option zum obigen Kommando hinzu, dass es funktioniert.

[Lösung Playbook](Auftrag_1_Loesung_Playbook.md)