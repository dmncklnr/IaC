Das Playbook könnte so aussehen:

```yaml
- name: My first play
  hosts: mymanagednodes
  tasks:
   - name: Create file /armin
     ansible.builtin.file:
       path: /armin
       state: touch
```

Der Aufruf muss dann wie folgt aussehen:

```
ansible-playbook -b -i /tmp/managednodes.inventory playbook.yml
```
