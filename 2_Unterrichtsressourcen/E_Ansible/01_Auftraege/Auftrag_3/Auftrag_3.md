# Auftrag 3

Die Steuerung von Playbooks mit Hilfe von Variablen für Gruppen und Hosts in Inventories ist sehr effizient. Dadurch werden dann die Playbooks kürzer und wiederverwendbar. Darum solltet ihr hier ein bisschen Zeit investieren, die verschiedenen Möglichkeiten der Gestaltung von Inventories zu verstehen.(1-2 Stunden).

Folgende Fragen sollten sie nachher beantworten können:

   - Wie erstelle ich Variablen pro Gruppe und Host?
   - Welcher Wert erhält eine Variable die in einer Gruppe und einem Host definiert ist?
   - In welcher Reihenfolge werden Variablen von Host und Gruppen zusammengeführt.
   - Wie kann ich Variablen mit Hilfe von Unterverzeichnissen und Files definieren?

Damit ihr das ganze besser versteht sollt ihr ein Inventory für eine Umgebung mit 3x2 Servern erstellen.

    - Diese sollen 3 Kunden (Migros, Coop, Nationalbank) gehoeren (1 Gruppe pro Kunde)
    - Pro Kunde soll es jeweils eine DB-Server und ein Web-Server geben.
    - Die Webserver und die DB-Server sollen jeweils einer Gruppe `webserver` und `dbserver` zugeordnet werden.

Ihr sollt jeweils pro Kunde Usernamen und Passwörter für den DB-Server sowie für die Web-Applikation definieren. Ihr sollt ein default-Usernamen und Passwort für DB-Server und Webserver definieren. Diese sollen vom Usernamen und Passwort die beim Kunden definiert sind überschrieben werden.

Jetzt sollt ihr bei einem der Kunden(Nationalbank) noch zusaetzliche Web- und DB-Server dazufügen, welche andere Passwörter verwenden sollen. Wie könnt ihr das ereichen. 

Um zu ueberprüfen ob die Variablen für einen Host jeweils richtig gesetzt sind, könnt ihr folgendes Kommando brauchen:
```bash
ansible-inventory --vars -i inventoryfile --host hostname
```
Der output wird ungefähr so aussehen
```json
{
    "ansible_host": "18.159.214.xxx",
    "ansible_user": "ubuntu",
    "username": "gr1",
    "password": "xyz"
}
```

[Lösung](./Auftrag_3_Loesung.md)