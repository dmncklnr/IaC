# Auftrag 5

Erstelle ein Playbook, welches 4 Apache Webserver installiert ([ansible.builtin.package](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/package_module.html)) und einen virtuellen Server `www.kunde.com` konfiguriert (Das kann mit einem [jinja-template-File](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html) gemacht werden). Diesen virtuellen Server sollst du durch Username und Passwort schützen.
Benutzte das [htpasswd module](https://docs.ansible.com/ansible/latest/collections/community/general/htpasswd_module.html) um das Passwort file zu erstellen.

Benutze das Inventory aus dem Auftrag 3, um das Playbook so zu gestalten, dass es für alle 4 Webserver funktioniert.

[Lösung Auftrag 5](Auftrag_5_Loesung.md)
