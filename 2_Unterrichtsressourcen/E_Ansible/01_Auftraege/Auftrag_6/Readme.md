# Auftrag 6: Installieren von MySQL

Der Kern von Ansible ist das geschickte "programmieren" von Playbooks, was in [Working with Playbooks](https://docs.ansible.com/ansible/latest/playbook_guide/index.html) sehr ausführlich beschrieben wird.

Hier geht es darum ein Playbook zu erstellen, welches zuerst mysql installiert und konfiguriert. Weil wir das reusable machen möchten , erstellen wir eine [Role](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_reuse_roles.html) `mysql`, welche 
- die Richtigen Packages für mariadb installiert 
- sicherstellt, dass mariadb richtig konfiguriert ist, d.h. erstellt ein ein Jinja-Template um Port und Netzwerk-Adresse zu konfigurieren (siehe /etc/mysql/mariadb.conf.d/50-server.cnf nachdem ihr mariadb installiert habt). 
- erstellt ein Handler welcher Maria-DB durchstartet wenn die Konfiguration geaendert wird.
- sicherstellt, dass der Service laueft

Dann möchten wir noch zusätzliche Datenbanken erstellen.

Erstelle eine Role, welches eine neue Datenbank konfiguriert. Benutze dazu
Collection von [community.mysql](https://docs.ansible.com/ansible/latest/collections/community/mysql/index.html). Sie soll:
- Die Datenbank erstellen
- einen User mit Zugriff auf die DB erstellen

Wenn ihr das alles gemacht habt, und euer Playbook funktioniert, dann schaut euch noch folgendes Repo von Faustin Lammer an [https://github.com/fauust/ansible-role-mariadb](https://github.com/fauust/ansible-role-mariadb)

Es gibt auch ein [Video 19:52](https://www.youtube.com/watch?v=CV8-56Fgjc0) von ihm, in welchem er seine Ansible Role demonstriert.
