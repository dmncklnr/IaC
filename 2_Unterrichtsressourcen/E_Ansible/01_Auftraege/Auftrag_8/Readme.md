# Auftrag 8: Loops und Conditionals

Dazu setzt ihr in der AWS-Academy-Umgebung eine Ubuntu-22.04 LTS-Umgebung auf.

In dieser sollt ihr folgende Aufgaben mit einem Ansible-Playbook erledigen:

1. Public-SSH-Key von Marcello und Armin für den ubuntu-User hinterlegen. Unsere Keys findet ihr [hier](./keys/). 
2. Erstellt folgende Ordnerstruktur im Home-Directory von ubuntu:
    ```
    docs
    docs/images
    src
    bin
    lib
    ```
3. Installiert die zusätzlichen Package `nginx`.
4. Erstellt ein File `/var/www/html/index.html` mit dem Inhalt:
    ```html
    <html>
    <body>
    <h1>Website</h1>
    </body>
    </html>
    ```
5. Das File `/var/www/html/index.html` soll folgende Berechtigungen und Besitzer haben:

    ```bash
    root@ip-172-31-48-212:~# ls -l /var/www/html/index.html 
    -rw-r----- 1 www-data www-data 0 Oct 27 19:31 /var/www/html/index.html
    ```
6. Erstellt ein File `$HOME/.bash_alias` für den Benutzer `ubuntu` und erzeugt in diesem einem Bash-Alias `tf` für `tail -f `. (Hinweis: .bash_alias wird automatisch beim login *gesourced* falls es existiert)

7. Erzeugt eine Gruppe `lehrer` und die Benutzer `marcello` und `armin`, die diese Gruppe als Primäre Gruppe haben.

8. Erzeugung von vielen Benutzern
    - Erzeugt eine Gruppe `students`
    - Erzeugt 26 Benutzer mit den Benutzernamen `studenta` bis `studentz` mit der Primären Gruppe  `students`.
    - Fügt ins authorzied_keys file [Marcello und Armin](./keys/) hinzu. 
    - Gebt diesen Benutzern das Recht die Webseite unter /var/www/html zu modifizieren, indem ihr diese zur Gruppe `www-data` hinzufuegt.


9. Löschen von Packages: 
   Löscht alle Packete deren Name mit `cloud-init` beginnt.

10. Erzeugt folgende Verzeichnisstruktur mit einem Kommando:

    ```
    /var/www/
    └── html
        ├── site1
        │   ├── bin
        │   ├── doc
        │   └── src
        ├── site10
        │   ├── bin
        │   ├── doc
        │   └── src
        ├── site2
        │   ├── bin
        │   ├── doc
        │   └── src
        ├── site3
        │   ├── bin
        │   ├── doc
        │   └── src
        ...
        ...
        └── site99
            ├── bin
            ├── doc
            └── src
    ```

11. Erzeugt in jedem Verziechnis `/var/www/html/siteX` ein File `index.html` mit folgendem  Inhalt:
    ```
    <html>
    <body>
    <h1>This is siteX</h1>
    </body>
    </html>
    ```
12. Owner und Gruppe aendern:
   - Aendert Owner und Gruppe zu `www-data` für /var/www/html und alles was unter diesem Pfad kommt.

13. Als zusätzliche Challenge sollen bei den Tasks 8,10 und 11 jeweils nur ein User bzw. eine Site geprüft werden, wenn beim Playbook-Aufruf --check mitgeben wird. Dabei soll zufällige ein User bzw. eine Site ausgesucht werden.

[Lösung](./ansible/howto.md)