- install ansible:
    ```
    python3 -m venv /tmp/venv_ansible
    . /tmp/venv_ansible/bin/activate
    pip install ansible
    ```
- update inventory file with ips of hosts to check:
    ```
    echo [ip of server1] > inventory
    echo [ip of server2] >> inventory
    .
    .
    .
    ```

- if you have never accessed a server to check you need to disable SSH-Host-Key-Checking:
  ```
  export ANSIBLE_HOST_KEY_CHECKING=False
  ```
- check the config with:
    ```
    ansible-playbook -i inventory playbook.yml --check --diff
    ```
    expected output:
    ```
    
    PLAY [all] *************************************************************************************************

    TASK [Gathering Facts] *************************************************************************************
    ok: [54.237.76.182]

    TASK [armin_pub_key] ***************************************************************************************
    ok: [54.237.76.182]
    .
    .
    .
    .
    TASK [User "{{item}}"] *************************************************************************************
    ok: [54.237.76.182] => (item=armin)
    ok: [54.237.76.182] => (item=marcello)

    PLAY RECAP *************************************************************************************************
    54.237.76.182              : ok=9    changed=0    unreachable=0    failed=0    skipped=2    rescued=0    ignored=0   
    ```
    Any oder state than `ok` and `skipped` is not ok. `skipped` should be exactly `2`
