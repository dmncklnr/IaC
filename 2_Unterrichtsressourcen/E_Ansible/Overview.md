# Overview

## Nodes (VMs, Laptops, Server, Router, ...)
Ansible besteht aus 2 Arten von Nodes:
- **Control Nodes:** Hier wird die Ansible-Software installiert. Hier müssen auch die Ansible-Konfiguration (Inventories und Playbooks) gespeichert oder mindest zugänglich sein. Hier muss ein unixbasierendes Betriebssystem laufen.
- **Managed Nodes oder Hosts:** Hier braucht es nur Software, damit man auf diese Nodes zugreifen kann und Skripts in Python (Unix) oder Powershell (Windows) ausführen kann.

## Inventory

Grundsätzlich werden hier die **Hosts**(Managed Nodes) mit IP oder Hostname oder FQDN erfasst.

Diese **Hosts** werden dann einer oder mehreren **Groups** hinzugefügt. Diese Gruppen erleichtern dann in den Playbooks die zuweisung von Plays zu Playbooks.

Zusätzlich werden hier noch **Variablen** gesetzt, die dann in Playbooks oder so gebraucht werden können. Es gibt einige Variablen, welche das verhalten von Ansible beeinflussen können, wie z.B den User (ansible_user) definieren mit welchem eingelogt werden soll.


## Playbooks

Playbooks bestehen Grundsätzlich aus mehreren **Plays**

### Plays

Jedem **Play** werden *Hosts*, bzw. *Groups* von
Hosts zugewiesen. Diese sind im *Inventory* definiert. 

In **Plays** wird definiert welche Tasks, Handler, Roles, ... dann ausgeführt werden.


![Overview](./X_gitressourcen/ansible_overview.drawio.png)

