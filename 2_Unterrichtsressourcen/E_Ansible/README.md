# Ansible

[TOC]

Ansible ist ein Open-Source-Automatisierungswerkzeug für die Bereitstellung und Verwaltung von IT-Infrastrukturen. Es ermöglicht die Automatisierung von Aufgaben ohne zusätzliche Agenten und bietet eine einfache YAML-Syntax für die Konfiguration. Ansible ist vielseitig einsetzbar und integriert sich gut mit verschiedenen Cloud-Plattformen und Konfigurationsmanagement-Tools. Es verbessert die Effizienz und Skalierbarkeit von Infrastrukturen.

## Installation

Die Installation geschieht in 2 Schritten.
Als erstes installiert man `python` und `pip` und dann kann man `ansible` installieren.

### Python
#### Windows
Installation von Python:

[Download Page](https://www.python.org/downloads/)

**Folgende Optionen für die Installation benutzen**:
- Standard-Installation, da diese gerade  `pip` mitinstalliert.

- Die `PATH`-Variable vom user gerade noch modifizieren lassen

#### MacOs

Beim Mac sollte `python` und `pip` vorinstalliert sein.

#### Linux

Je nach Distro die benutzt wird muss man noch `pip` nachinstallieren, z.B. für Ubuntu:
```
sudo apt update
sudo apt install python3-pip
sudo apt install python-is-python3
```

### `pip`-Upgrade auf letzte Version

Normalerweise sollte man als erstes `pip` auf den letzten stand bringen:


```
sudo python -m pip install --upgrade pip
```

### Ansible-Installation

```
sudo su -  # Nur für Linux VMs
pip install ansible
exit       # Nur für Linux VMs
```
Danach kann man die Version mit
```
ansible --version
```
überprüfen.

**Achtung**: Bei Windows muss man noch `Windows Subsystem for Linux (WSL)`  aktivieren.
Sonst gibt es folgenden Fehler:
```
AttributeError: module 'os' has no attribute 'get_blocking'
```
## Ansible Einführung

Zuerst ein kurzes [Video](https://www.youtube.com/watch?v=4PPwoqPeKtY), mit Anmerkungen von mir.

[Overview](./Overview.md)

## Erste Schritte 
Das [Gettings Startet Tutorial](https://docs.ansible.com/ansible/latest/getting_started/index.html) aus der Ansible Dokumentation ist recht gut, d.h. ich habe mir so Ansible innerhalb kürzester Zeit beigebracht. 

Vorgehen:
ersten Schritte Anhand von [Buildung an Inventory](https://docs.ansible.com/ansible/latest/getting_started/get_started_inventory.html#building-an-inventory) und von [Creating a Playbook](https://docs.ansible.com/ansible/latest/getting_started/get_started_playbook.html#creating-a-playbook) machen. (Dauer 2-4 Stunden)

### [Auftrag 1](./01_Auftraege/Auftrag_1.md)

## Ansible Deepdive

### Auftrag 2
Konzepte verstehen Anhand von [Ansible Concepts](https://docs.ansible.com/ansible/latest/getting_started/basic_concepts.html#ansible-concepts), ohne auf jeden Link zu klicken (20 min)

### Auftrag 3
Lerne anhand von [How to build your inventory](https://docs.ansible.com/ansible/latest/inventory_guide/intro_inventory.html#intro-inventory) wie man Inventories baut. 

[ Hands on ](./01_Auftraege/Auftrag_3/Auftrag_3.md) zum üben der Theorie.


### Auftrag 4

Lesen sie die [Einführung in Ansible Playbooks](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_intro.html#about-playbooks). (20 min)

### Auftrag 5
Lerne wie man ein Playbook für mehrere Instanzen vom selben brauchen kann.

[ Hands on ](./01_Auftraege/Auftrag_5/Auftrag_5.md)


### Auftrag 6 : MariaDB mit Role aufsetzen
[ Hands on ](./01_Auftraege/Auftrag_6/Readme.md)

### Auftrag 7: AWS-Plugin Inventory

[Link](./01_Auftraege/Auftrag_7/Readme.md)


### Auftrag 8: Loops und Conditionals

Manchmal ist es nötig mehrere User, Virtuelle Webserver, ... aufzusetzen, oder gewisse Tasks nur unter bestimmten Bedingungen auszuführen. Dazu gibt es [Loops](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_loops.html) und [Conditionals](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_conditionals.html). Benutzt diese um diesen [Auftrag](./01_Auftraege/Auftrag_8/Readme.md) zu erfüllen.


### Auftrag 9: Den "output" von Tasks speichern und benutzen.

#### Theorie
Bis jetzt habt ihr gelernt wie man [Variablen in Inventories, Playbooks, Roles](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_variables.html) setzen kann.

Wir haben auch gesehen, dass viele Variabeln automatisch gesetzt werden als [Facts](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_vars_facts.html)

Variabeln kann man aber auch aus dem "output" eines Tasks setzen. Dazu benutzt man das Key-Word `register`. Um das geht is in diesem Kapitel

z.B: das Playbook macht immer als erster task ein Backupfile `myapp.conf.20170930T132646` des Configfiles `myapp.conf` anpasst. Wir möchten aber nur die letzten 20 backups behalten:

```ansible
- name: Testplaybook
  hosts: localhost
  vars:
    configfile: myapp/myapp.conf
  connection: local
  tasks:
  - name: make backup file
    shell:
      cmd: "cp {{configfile}} {{configfile}}.{{ansible_date_time.iso8601_basic_short}}"
  # Hier kommt was auch immer getan werden muss mit dem configfile
  # Hier kommt der Code zum Cleanup:
  - name: get the list of files ordered by timestamp
    shell:
      cmd: "ls -1t {{configfile}}.*"
    check_mode: no
    ignore_errors: true
    register: lsoutput
  # lsoutput hat hier ganz file felder die man mit debug anschauen kann
  - debug:
      msg: "{{lsoutput}}"
  # wir waehlen lsoutput.stdout_lines, da wir dann einfach eine liste machen koennen die alle 
  # filenamen auser den ersten 20 enthaelt
  - name: remove files all files except the last 5
    file:
      path: "{{item}}"
      state: absent
    loop: "{{lsoutput.stdout_lines[20:]}}"
  - debug:
      msg: "{{removeoutput}}"
```

#### Auftrag

Hier geht es zum Auftrag in welchem ihr lernt wie `register` benutzt wird [Auftrag: Register output](./01_Auftraege/Auftrag_9/Readme.md)


### Auftrag 10: Error Handling

Beim Error-Handling in Ansible geht es um folgendes:

- Definieren ob das Playbook weiterfahren kann wenn ein Task **failed** ist mit `ingore_errors` und `ignore_unreachable`. [Ignore Failed Commands](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_error_handling.html#ignoring-failed-commands)
- Definieren wann ein Task **failed** mit `failed_when`. [Defining failure](./https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_error_handling.html#defining-failure)
- Definieren wann ein Task als **changed** gelten soll mit `changed_when`. [Defining "changed"](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_error_handling.html#ignoring-failed-commands)
- [Rescue Tasks definieren](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_blocks.html#block-error-handling), welche ausgeführt werden wenn ein Tasks eines Blocks **failed**

Als erstes möchte ich klarstellen, dass allzu extensives Error-Handling wahrscheinlich nicht zielführend ist. Aber manchmal ist es notwendig. Zum Beispiel in folgenden fällen:

- Vor einer Anpassung des Codes muss man einen wohldefinierten Ausgangszustand herstellen, d.h. zum Beispiel Applikationen stoppen und Deinstallieren von Packages. Dabei kann aus verschiedenen **schon bekannten** Gründen zu Problemen kommen.
- Es gibt häufig temporäre Probleme im Netzwerk, beim Cloudprovider (Migrosoft), welche sich meistens von selbst lösen.

Falls sich solche Probleme mit **gezielten Aktionen** oder durch **erneutes Versuchen** meistens lösen lassen, ist es sinnvoll Error-Handling zu betreiben.

Manchmal will man gewisse Fehler auch einfach ignorieren, weil sie kein Problem für den weiteren Verlauf des Playbooks darstellen.

[Auftrag: Fehler-Handling](./01_Auftraege/Auftrag_10/Readme.md)


### Auftrag 11: AWS Setup VM

Ansible kann in AWS ziemlich jede Resource erzeugen, abfragen und ändern.

Dabei gibt es Module die nur dazu da sind Information vom AWS auzulesen. Diese Module heissen normalerweise ...._info wie z.B:

- amazon.aws.ec2_vpc_endpoint_info: Gib VPC mit Infos aus
- amazon.aws.ec2_vpc_subnet_info: Gin Subnetze mit Infos aus
- ...

Dann gibt es Module zum erzeugen und ändern von AWS-Resourcen wie z.B.:

- amazon.aws.ec2_instance
- amazon.aws.ec2_security_group
- ..

Dies geben als "output" immer die Informationen der erzeugten bzw. geänderten Resourcen zurück.

Die Doku der AWS-Module findet ihr [hier](https://docs.ansible.com/ansible/latest/collections/amazon/aws/index.html).


Hier geht es zum [Auftrag](./01_Auftraege/Auftrag_11/Readme.md)

### Auftrag 12: Azure Setup VM

Im [Auftrag](./01_Auftraege/Auftrag_12/Readme.md) sollt ihr einfach eine VM erzeugen mit Ansible auf der Azure.

### Lernziele / Taxonomie 

*Ersetzen mit einer Liste von Inhalten welche durchgenommen werden*

### Transfer

### Hands-on

*Verweise auf Unterverzeichnis mit Hands-on*

### Links

*Links zu den Unterthemen*### Kompetenzen

*Aufzählen der Handlungskompetenzen die in dieser Kompetenz behandelt werden, z.B. A2.3, B5.1*

### Lernziele / Taxonomie 

*Ersetzen mit einer Liste von Inhalten welche durchgenommen werden*

### Transfer

### Hands-on

*Verweise auf Unterverzeichnis mit Hands-on*

### Links

*Links zu den Unterthemen*

## Links

*Allgemeine Links welche sich nicht in die Unterthemen einordnen lassen*

