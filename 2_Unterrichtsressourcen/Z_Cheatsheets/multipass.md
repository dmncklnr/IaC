[TOC]

# Erstellen einer VM
```commandline
multipass launch -n namedervm
```
# Erstellen einer VM und uebergeben eines Cloudinit-files
```commandline
multipass launch -n namedervm --cloudinit whereever/cloudinit.yml
```
# Auflisten aller VMs
```commandline
multipass ls
```
# Verbinden mit einer multipass VM als default User:
```commandline
multipass shell vmname
```
# Transferieren eines Files von und zu VM
Lokales File `/tmp/xyz` auf VM `vmname` ins Home-Verzeichnes des Default-Users transferieren:
```commandline
multipass transfer /tmp/xyz vmname:
```
File xyz in im Home-Verzeichnes des Default-Users auf der VM `vmname` ins lokale Verzeichnis `/tmp` kopieren:
```commandline
multipass transfer vmname:xyz /tmp
```
Lokales File `/tmp/xyz` auf VM `vmname` unter dem Filenamen `/tmp/zyx` ablegen:
```commandline
multipass transfer /tmp/xyz vmname:/tmp/zyx
```

# Loeschen einer VM
Erster Schritt stopen
```commandline
multipass stop vmname
```
Als deleted markieren
```commandline
multipass delete vmname
```
Dann entgueltig loeschen inklusive Diskplatz freigeben aller vms die im status deleted sind
```commandline
multipass pruge
```


# Lokales Verzeichniss in VM mounten

Wenn ich das lokale HOME-Verzeichnis `$HOME` in der VM `jinja` unter `/mnt` zur Verfügung stellen möchte, kann ich dies wie folgt tun:

```bash
multipass mount $HOME jinja:/mnt
```
