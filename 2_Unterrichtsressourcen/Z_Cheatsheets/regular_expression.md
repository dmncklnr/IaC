# Regular Expressions oder kurz Regex

## Warum brauchen wir Regex?   
Wenn wir aus einem Text gewisse Felder herausfiltern wollen, die nicht immer an derselben Stelle sind.
Diese können zwischen an verschiedenen Stellen zwischen verschiedenen Wörtern in verschiedenen Reihenfolgen vorkommen.
Einfach gesagt: Ich könnte euch als Mensch erklären wie ihr das Feld im Text findet, aber einen Computer zu programmieren scheint
nicht einfach zu sein. Dann brauchen wir Regular Expressions.

## Was ist eine Regex?
Eigentlich ist es eine Beschreibungssprache fuer wie man nach Mustern suchen soll.

# Cheatsheets
[Cheatsheet by datacamp.com](https://images.datacamp.com/image/upload/v1665049611/Marketing/Blog/Regular_Expressions_Cheat_Sheet.pdf) 
[Cheatsheet by Mozilla](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions/Cheatsheet) 

# Online Regex-Testing-Sites
[https://regex101.com/](https://regex101.com/) 
[https://regexr.com/](https://regexr.com/)