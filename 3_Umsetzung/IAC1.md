# IAC1 - Infrastructure as Code 1

 - Bereich: System- und Netzwerkarchitektur bestimmen
 - Semester: 1

## Lektionen: 

* Präsenz: 38
* Virtuell: 28
* Selbststudium: 39

## Voraussetzungen:

Linux-Grundkenntnisse

## Dispensation

## Technologien

- [multipass.run](https://multipass.run/)
- [AWS Academy](https://awsacademy.instructure.com/login/canvas)

## Methoden: 

Praktische Laborübungen mit Coaching durch Lehrperson

## Schlüsselbegriffe: 

Automatisieren,Cloud-Init,Ansible,Bash,YAML

## Lerninhalte
 - Toollandschaft in der Infrastrure as Code kennenlernen 
 - Cloud-Init Module kennenlernen und einsetzen
 - Cloud-Init uns Sicherheit
 - Cloud-Init Internals
 - Grundlagen in BASH-Programmierung
 - bessere Skripte programmieren
 - Erste Schritte in Ansible

## Übungen und Praxis

 - Provisionieren von Virtuellen Rechnern mit Cloud-Init
 - Deployen von Komplexen Applikationen mit Cloud-Init und Bash
 - Cloud-Init sicher machen
 - Cloud-Init-Ablauf modifizieren
 - Abläufe mit Bash-Skripten programmieren
 - Ansible aufsetzen



## Lehr- und Lernformen: 

Lehrervorträge, Lehrgespräche, Workshop, Gastreferenten, Einzel- und Gruppenarbeiten, Lernfragen, Präsentationen, Laborübungen



## Lehrmittel:  

- [Bash](https://linuxconfig.org/bash-scripting-tutorial-for-beginners)
- [Ansible Core](https://docs.ansible.com/ansible-core/devel/index.html)
- [Cloud-Init](https://cloudinit.readthedocs.io/en/latest/)

## Hilfsmittel:

- Rahmenlehrplan 



