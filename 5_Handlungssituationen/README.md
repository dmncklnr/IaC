# Handlungsziele und Handlungssituationen 

Zu jedem einzelnen Handlungsziel aus der Modulidentifikation wird eine realistische, beispielhafte und konkrete Handlungssituation beschrieben, die zeigt, wie die Kompetenzen in der Praxis angewendet werden.

### Aktuelle technologiebasierte Entwicklungswerkzeuge einsetzen 

Es soll eine Bestehende Applikationsumgebung auf in die Cloud verschoben werden. Dazu möchte man den Prozess der Migration weitgehend automatisieren.

Mit Hilfe geeigneter Entwicklungswerkzeuge, kann der Installationsprozess einer VM weitgehend automatisiert werden.

Auch sind kleinere Tools zur Datenmigration zu schreiben, die im Cut-Over benutzt werden um die Migration wiederholbar,fehlerfrei und schnell durchführen zu können.

### Informationsquellen und Wissensnetzwerke kritisch reflexiv nutzen 

Es soll ein neues Tool für das zentrale Konfigurationsmanagement evaluiert werden.

Dazu muss Internetrecherche betrieben werden um vorhandene Tools zu suchen und nach vorgegebenen Kriterien zu beurtielen. Die kritische Auseinandersetzung mit den Quellen ist dabei entscheidend.


### 
