![TBZ Logo](./x_gitressourcen/tbz_logo.png) 

# IAC - Infrastructure as Code

## Kurzbeschreibung des Moduls 

In diesem Modul werden Methoden zur Automatisierung von Bereitstellung von Unix-Umgebungen inklusive Services erlernt. Dazu gehören automatisierung mit Skripten, wie auch Beschreibung von Installationen mittels Cloud-Init und Ansible. Erste Schritte zur Automatisierung mit CICD-Pipelines sind auch Thema.

## Unterrichtsressourcen

- [Übersicht Infrastructure as Code (IaS)](2_Unterrichtsressourcen/A_Uebersicht_IaS/)
- [Toolslandschaft IaS](2_Unterrichtsressourcen/B_Toollandschaft_IaS/)
- [Automatisieren mit Bash](2_Unterrichtsressourcen/C_Bash/)
- [Cloud-Init](2_Unterrichtsressourcen/D_Cloud-Init/)
- [Ansible](2_Unterrichtsressourcen/E_Ansible/)
- [CI/CD-Pipelines](./2_Unterrichtsressourcen//G_CICD/)

## Angaben zum Transfer der erworbenen Kompetenzen 

Umsetzung der erlernten Theorie in Einzel- und Gruppenarbeiten.

## Abhängigkeiten und Abgrenzungen


### Vorangehende Module

- MAAS

### Nachfolgende Module

- AWS
- Azure

## Dispensation

Voraussetzung für eine Dispensiation sind:

- mittlere BASH-Skripting Projekte umgesetzt
- mehrere Cloud-Init Deployments mit private GIT-Repos und anderen Externen Resourcen, Integration von Tools wie puppet, ansible
- gute Kenntnisse eines Konfigurationsmanagement-Tools wie puppet,chef,ansible

## Unterlagen zum Modul

### Organisatorisches

[Organisatorisches ](0_Organisatorisches)zur Autorenschaft dieser Dokumente zum Modul

## Handlungskompetenzen

[Handlungskompetenzen](1_Handlungskompetenzen) zu den Handlungszielen 

### Unterrichtsressourcen

[Unterrichtsressourcen](2_Unterrichtsressourcen) von Aufträgen und Inhalten zu den einzelnen Kompetenzen

### Umsetzung

[Umsetzung](3_Umsetzung) 

### Fragekatalog

[Fragekatalog ](4_Fragekatalog) Allgemein

### Handlungssituationen

[Handlungssituationen ](5_Handlungssituationen)mit möglichen Praxissituationen zu den einzelnen Handlungszielen

### Zertifizierungen 

Mögliche [Zertifizierungen](8_Zertifizierungen) für dieses Modul 

- - - 

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/ch/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Schweiz Lizenz</a>.

- - - 
